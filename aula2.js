//Yuri Nogueira Ressiguier
//Exercício JS - 2 (30/07)

/* Fazer uma função que retorne os alunos com as medias mais altas de cada turma, dada uma coleção de objetos 
EX: */
var alunos = 
[
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Yuri",
        "turma": "A",
        "nota1": 8,
        "nota2": 7
    },
    {
        "nome": "Kaio",
        "turma": "A",
        "nota1": 10,
        "nota2": 9
    },
    {
        "nome": "Thiago",
        "turma": "B",
        "nota1": 10,
        "nota2": 3
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Debora",
        "turma": "C",
        "nota1": 10,
        "nota2": 5
    }
]
//Inicia as variáveis da maior média
let maiorMediaA = 0;
let maiorMediaB = 0;
let maiorMediaC = 0;
//Inicia as variáveis do nome com a maior média
let nomeNerdA = "";
let nomeNerdB = "";
let nomeNerdC = "";

function mediasAlunos(alunos){
    for (i in alunos){
        media = 0;                          //Reseta a média
        let nome = alunos[i]["nome"];       //Pega o nome
        let turma = alunos[i]["turma"];     //Pega a turma
        let nota1 = alunos[i]["nota1"];     //Pega a nota1
        let nota2 = alunos[i]["nota2"];     //Pega o nota2
        media = (nota1 + nota2) / 2;        //Faz a média
        switch (turma) {                    //Separa os alunos em turmas
            case "A":
                if (media > maiorMediaA){
                    nomeNerdA = nome;       //Guarda os maiores valores
                    maiorMediaA = media;
                }
                break;
            case "B":
                if (media > maiorMediaB){
                    nomeNerdB = nome;
                    maiorMediaB = media;
                }
                break;
            case "C":
                if (media > maiorMediaC){
                    nomeNerdC = nome;
                    maiorMediaC = media;
                }
                break;
        }
    }
    console.log("O aluno(a) "+nomeNerdA+ " teve a média mais alta da turma A, com "+maiorMediaA);
    console.log("O aluno(a) "+nomeNerdB+ " teve a média mais alta da turma B, com "+maiorMediaB);
    console.log("O aluno(a) "+nomeNerdC+ " teve a média mais alta da turma C, com "+maiorMediaC);
}
mediasAlunos(alunos);

/* Resultado: 
O aluno(a) Kaio teve a média mais alta da turma A, com 9.5  
O aluno(a) Thiago teve a média mais alta da turma B, com 6.5
O aluno(a) Jonathan teve a média mais alta da turma C, com 9 */